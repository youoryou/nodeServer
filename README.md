# nodeServer

#### 项目介绍
nodejs server

#### 软件架构
软件架构说明


#### 安装教程

1. 安装最新版的nodejs
修改C:\Program Files\nodejs\node_modules\npm\npmrc文件
prefix=C:\Program Files\nodejs\node_global
cache=C:\Program Files\nodejs\node_cache
并新建对应这二个目录,把node_global 这个目录加入环境变量里
2. 在当前项目目录下操作 npm install -g typescript
npm init -y 在当前目录下生成package.json
下载其它模块npm install @types/node -save
tsc --init 在当前目录下生成tsconfig.json
安装插件tslint 安装前先装npm install -g tslint typescript
如果版本不对请升级npm install -g tslint@latest
再安装最新的vscode
tsconfig.json的文档
https://www.typescriptlang.org/docs/handbook/tsconfig-json.html
3. 在当前项目目录下执行
node -v
npm -v
tsc -v
看看环境是否装好
4. https://code.visualstudio.com/docs/editor/tasks
vscode 任务->运行任务 
后面需要安装的js包在这里http://npm.taobao.org/package/@types/node  npm install --save @types/mysql
#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

