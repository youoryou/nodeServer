
import {App,AppState} from './core/App';
import {Utils} from './core/utils/utils';
import {Log} from './core/log/log';

import {LogServer} from './LogServer/LogServer';

Log.instance.initLog("testServer");
Log.instance.trace("11111111111111");
Log.instance.debug("22222222222");
Log.instance.info("333333333333");
Log.instance.warn("4444444444444");
Log.instance.error('55555555555555');
Log.instance.fatal('66666666666666');
let a:number[] = Utils.make_random(3,1,3);
a.forEach((i)=>{
    Log.instance.info(i);
});

let b:number = Utils.make_one_random(1,2);
Log.instance.info(b);

console.log(AppState[AppState.eConstruct]);
let a1 = Utils.genGuid(1);

if(Utils.isTheSameDayByNow(Utils.getCurTime(), 5*60*60)){
    console.log("the same day");
}

process.on('uncaughtException', function(err) {
    Log.instance.error('Caught exception: %s' + err.stack);
});

let logServer = new LogServer();
logServer.startMainLoop();
