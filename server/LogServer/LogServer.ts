import {App,AppState} from '../core/App';
import {ServerType,ServerEngineSetup} from '../core/net/ServerEngineSetup';
import {ServerEngine} from '../core/net/ServerEngine';
import {ServerCfg} from '../core/cfg/serverCfg';
import {TimerManager} from '../core/utils/TimerManager';

export class LogServer extends App{
    private quit:boolean;
    public constructor(){
        super();
        this.quit = false;
    }

    public onInit():number{
        console.log("start init logserver");
        let serverAppSetup = <ServerEngineSetup>{};
        serverAppSetup.nServerType = ServerType.Log;
        serverAppSetup.fServerRegistered = this.onServerRegistered;
        ServerEngine.instance.initServer(serverAppSetup);

        let config = ServerCfg.getServerConf("LogServer");

        //test
        TimerManager.registerTimer(1000, this, this.test, null, false);

        return AppState.eRunning;
    }

    private test():void{
        console.log("2222222 %d", 1);
    }

    public onRunning():number{
        // console.log("logserver running");

        this.quit = this.quit || ServerEngine.instance.shutDownRequested();
        return this.quit ? AppState.eCleanup : AppState.eRunning;
    }

    public onCleanup():number{
        console.log("logserver cleanup");
        
        
        return AppState.eDestroy;
    }

    private onServerRegistered():void{

    }
}