import * as log4js from 'log4js';

let logCfg = require('../../../cfg/log4js.json');
export class Log{
    private sLogName:string;
    private static _instance:Log;
    public constructor(){
        this.sLogName = "testServer";
    }

    public static get instance(): Log {
        if (Log._instance == null) {
            Log._instance = new Log();
        }
        return Log._instance;
    }

    public initLog(sLogName:string):void{
        this.sLogName = sLogName;
        log4js.configure(logCfg);
    }

    private log(logType:string, message: any, ...args: any[]):void{
        let logger = log4js.getLogger(this.sLogName);
        if(logger){
            switch(logType){
                case 'trace':
                    logger.trace(message, args);
                    break;
                case 'debug':
                    logger.debug(message, args);
                    break;
                case 'info':
                    logger.info(message, args);
                    break;
                case 'warn':
                    logger.warn(message, args);
                    break;
                case 'error':
                    logger.error(message, args);
                    break;
                case 'fatal':
                    logger.fatal(message, args);
                    break;
                default:
                    return;
            }
        }
    }

    public trace(message:any, ...args:any[]):void{
        this.log('trace', message, args);
    }

    public debug(message:any, ...args:any[]):void{
        this.log('debug', message, args);
    }

    public info(message:any, ...args:any[]):void{
        this.log('info', message, args);
    }

    public warn(message:any, ...args:any[]):void{
        this.log('warn', message, args);
    }

    public error(message:any, ...args:any[]):void{
        this.log('error', message, args);
    }

    public fatal(message:any, ...args:any[]):void{
        this.log('fatal', message, args);
    }    

}


