
let cfg = require('../../../cfg/serverConfig.json');

export class ServerCfg{
    constructor(){

    }

    public static getServerConf(sServer:string):any{
        return cfg[sServer];
    }

    public static getServerId():number{
        return cfg.ServerId
    }

}
