
class TimerContext{
    public cbObject:any;
    public cb:Function = function() {};
    public args:any = undefined;
    public expireTime:number = 0;
    public interval:number = 0;
    public loop:any = false;
    public deleted:boolean = false;
    public expiatory:any = false;
}

export class TimerManager{
    private static _instance:TimerManager;
    private timeList:TimerContext[] = [];
    private lastTime:number = 0;
    constructor(){
    }

    public static get instance():TimerManager{
        if(!TimerManager._instance){
            TimerManager._instance = new TimerManager();
        }
        return TimerManager._instance;
    }

    public static registerTimer(time:number, cbObject:any, cb:Function, args?:any, loop?:boolean, expiatory?:boolean):any{
        return TimerManager.instance.registerTimer(time, cbObject, cb, args, loop, expiatory);
    }

    public static unregisterTimer(handler:any):boolean{
        return TimerManager.instance.unregisterTimer(handler);
    }

    public static startGlobalTimer():void{
        TimerManager.instance.timerCallBack();
    }

    public static sleep(nSleepTime:number):void{
        let nStartTime = Date.now();
        while(Date.now() - nStartTime <= nSleepTime){

        }
    }

    private registerTimer(time:number, cbObject:any, cb:Function, args?:any, loop?:boolean, expiatory?:boolean):any{
        let context = new TimerContext();
        context.expireTime = Date.now() + time;
        context.interval = time;
        context.cbObject = cbObject;
        context.cb = cb;
        context.args = args;
        context.loop = loop;
        context.expiatory = expiatory;
        this.timeList.push(context);
        return context;
    }

    private unregisterTimer(handler:any):boolean{
        let context = <TimerContext>(handler);
        if(!context.deleted){
            let index = this.timeList.indexOf(context);
            if(index >= 0){
                context.deleted = true;
                return true;
            }
        }
        return false;
    }

    private timerCallBack():void{
        let now = Date.now();
        let nDiffTime = now - this.lastTime;
        let nCount = this.timeList.length;
        if(nDiffTime < 1000 || nCount <= 0){
            return;
        }
        this.lastTime = now;
        
        let context:TimerContext;
        for(let i=nCount-1;i>=0;--i){
            context = this.timeList[i];
            if(context.deleted){
                this.timeList.splice(i, 1);
            }else if(context.expireTime <= now){
                if(!context.args || !context.args.unshift){
                    context.cb.call(context.cbObject, context.args);
                }else{
                    context.cb.apply(context.cbObject, context.args);
                }
                if(!context.deleted && context.loop){
                    if(context.expiatory){
                        context.expireTime += context.interval;
                        let c = 500;
                        while(context.expireTime < now && c > 0){
                            if(!context.args || !context.args.unshift){
                                context.cb.call(context.cbObject, context.args);
                            }else{
                                context.cb.apply(context.cbObject, context.args);
                            }
                            context.expireTime += context.interval;
                            --c;
                        }
                    }else{
                        context.expireTime = now + context.interval;
                    }
                }else{
                    context.deleted = true;
                }
            }
        }
    }


}