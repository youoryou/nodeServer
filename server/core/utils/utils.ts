import {ServerCfg} from '../cfg/serverCfg';

export class Utils{
    public constructor(){

    }

    public static getCurTime():number{
        return Math.floor(Date.now()/1000);
    }

    public static getCurMsTime():number{
        return Date.now();
    }

    //serverId seriesId time  (seriesId 由各自模块去负责)
    public static genGuid(nSeriesId:number):number{
        let nServerId = ServerCfg.getServerId();
        let nCurTime = this.getCurTime();
        return (nServerId << 52) | (nSeriesId << 32) | (nCurTime << 0);
    }

    public static genServerId(nSeriesId:number, nType:number):number{
        let nServerId = ServerCfg.getServerId();
        return (nServerId << 52) | (nSeriesId << 32) | (nType << 0);
    }

    public static isTheSameDayByNow(nTimePoint:number, nSecond:number):boolean{
        let date = new Date();
        let offset = date.getTimezoneOffset() * 60;
        let nCurTime = this.getCurTime();
        let day1 = (nTimePoint - offset - nSecond)/86400;
        let day2 = (nCurTime - offset - nSecond)/86400;
        if(Math.floor(day1) === Math.floor(day2)){
            return true;
        }else{
            return false;
        }
    }

    //判断某个数据是否在数组内
    public static contains(array:number[], n:number):boolean{
        for(let i=0;i<array.length;i++){
            if(array[i] == n){
                return true;
            }
        }

        return false;
    }

    //生成n个大于等于 min 小于等于 max 不重复随机数
    public static make_random(n:number, min:number, max:number):number[]{
        let nums:number[] = [];
        if(n < 0 || min > max){
            return nums;
        }

        if( n > (max - min + 1)){
            for(let i = min; i <= max; i++){
                nums.push(i);
            }
            return nums;
        }

        if(min <= max){
            while(true){
                if(nums.length >= n){
                    break;
                }
                let num = Math.floor(min + Math.random() * (max - min + 1));
                if(!Utils.contains(nums, num)){
                    nums.push(num);
                }
            }
        }

        return nums;
    }

    //生成1个大于等于min 小于等于 max 随机数
    public static make_one_random(min:number, max:number):number{
        let num:number = -1;
        if(min <= max){
            num = Math.floor(min + Math.random() * (max - min + 1));
        }
        return num;
    }


    
}


