
export class RunLoop{
    private static _instance:RunLoop;
    private nCurId:number;
    private mCallBacks:{[id:number]:any} = {};
    private mToAdd:{[id:number]:any} = {};
    private vToRemove:{[id:number]:number} = {}
    constructor(){
        this.nCurId = 0;
    }

    public static get instance():RunLoop{
        if(!RunLoop._instance){
            RunLoop._instance = new RunLoop();
        }
        return RunLoop._instance;
    }

    public run():void{
        this.remCallBack();
        this.addCallBack();
        for(let id in this.mCallBacks){
            if(this.mCallBacks[id]){
                this.mCallBacks[id]();
            }
        }
        this.remCallBack();
        this.addCallBack();
    }

    public add(callBack:Function):number{
        let newId = ++this.nCurId;
        this.mToAdd[newId] = callBack;
        return newId;
    }

    public remove(id:number):void{
        if(!this.vToRemove[id] && (this.mCallBacks[id] || this.mToAdd[id])){
            this.vToRemove[id] = id;
        }
    }

    private addCallBack():void{
        for(let id in this.mToAdd){
            let cb = this.mToAdd[id];
            this.mCallBacks[id] = cb;
        }
        this.mToAdd = {};
    }

    private remCallBack():void{
        for(let index in this.vToRemove){
            let nLoopId = this.vToRemove[index];
            if(this.mCallBacks[nLoopId]){
                delete this.mCallBacks[nLoopId];
            }else if(this.mToAdd[nLoopId]){
                delete this.mToAdd[nLoopId];
            }
        }
        this.vToRemove = {};
    }

}