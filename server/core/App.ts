import {Utils} from './utils/utils';
import {TimerManager} from '../core/utils/TimerManager';
import {RunLoop} from './utils/RunLoop';

export enum AppState{
    eConstruct,
    eEnqueuePreload,
    ePreloading,
    eInit,
    eRunning,
    eCleanup,
    eDestroy,
    eBlocked,
    
    eNumAppStates,
    eInvalidAppState,
}
    
export class App{
    private curState:AppState;
    private nextState:AppState;
    private blockers:number[];
    private quitRequested:boolean;
    private suspendRequest:boolean;

    public constructor(){
        this.curState = AppState.eConstruct;
        this.nextState = AppState.eInvalidAppState;
        this.quitRequested = false;
        this.suspendRequest = false;
        this.blockers = [];
    }

    public requestSuspend():void{
        this.nextState = AppState.eCleanup;
        this.suspendRequest = true;
    }

    public requestQuit():void{
        if(this.curState == AppState.eRunning){
            this.nextState = AppState.eCleanup;
        }else{
            this.nextState = AppState.eDestroy;
        }
        this.quitRequested = true;
    }

    public onConstruct():number{
        console.log("super construct");
        return AppState.eEnqueuePreload;
    }

    public onEnqueuePreload():number{
        console.log("super enqueuepreload");
        return AppState.ePreloading;
    }

    public onPreloading():number{
        console.log("super superpreloading");
        return AppState.eInit;
    }

    public onInit():number{
        console.log("super init");
        return AppState.eRunning;
    }

    public onRunning():number{
        console.log("super running");
        return AppState.eCleanup;
    }

    public onCleanup():number{
        console.log("super cleanup");
        if(this.quitRequested){
            return AppState.eDestroy;
        }else if(this.suspendRequest){
            this.addBlocker(AppState.eInit);
            return AppState.eInit;
        }else{
            return AppState.eDestroy;
        }
    }

    public onDestroy():number{
        console.log("super destroy");
        return AppState.eInvalidAppState;
    }

    public addBlocker(nState:number):void{
        if(!Utils.contains(this.blockers, nState)){
            this.blockers.push(nState);
        }
    }

    public removeBlocker(nState:number):void{
        let nIndex = this.blockers.indexOf(nState);
        if(nIndex > -1){
            this.blockers.slice(nIndex, 1);
        }
    }

    public startMainLoop():void{
        console.log("start main loop");
        while(this.curState != AppState.eInvalidAppState){
            this.onFrame();
        }
    }

    private onFrame():void{
        if((this.nextState != AppState.eInvalidAppState) && (this.nextState != this.curState)){
            if(Utils.contains(this.blockers, this.nextState)){
                if(this.curState != AppState.eBlocked){
                    this.curState = AppState.eBlocked;
                }
            }else{
                this.curState = this.nextState;
                this.nextState = AppState.eInvalidAppState;
            }
        }

        if(AppState.eBlocked == this.curState){
            
        }else{
            
            // TimerManager.startGlobalTimer();
            RunLoop.instance.run();

            switch(this.curState){
                case AppState.eConstruct:
                    this.nextState = this.onConstruct();
                    break;
                case AppState.eEnqueuePreload:
                    this.nextState = this.onEnqueuePreload();
                    break;
                case AppState.ePreloading:
                    this.nextState = this.onPreloading();
                    break;
                case AppState.eInit:
                    this.nextState = this.onInit();
                    break;
                case AppState.eRunning:
                    this.nextState = this.onRunning();
                    break;
                case AppState.eCleanup:
                    this.nextState = this.onCleanup();
                    break;
                case AppState.eDestroy:
                    this.nextState = this.onDestroy();
                    this.curState = AppState.eInvalidAppState;
                    break;
                default:
                    break;
            }
        }
    }
}

