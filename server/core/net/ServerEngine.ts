import {ServerType,ServerEngineSetup,ServerEngineState} from '../../core/net/ServerEngineSetup';

import {TimerManager} from '../../core/utils/TimerManager';
import {RunLoop} from '../utils/RunLoop';
import {ServerManager} from './ServerManager';
import * as net from 'net';

import {ServerCfg} from '../../core/cfg/serverCfg';
// let serverCfg = require('../../../cfg/serverConfig.json');

export class ServerEngine{
    private static _instance:ServerEngine;
    private bShutDown:boolean;
    private nServerType:number;
    private nState:number;

    private fServerDisconnected:any;
    private fServerRegistered:any;
    private fServerConnected:any;
    private fServerStarted:any;
    private fServerListener:any;
    public constructor(){
        this.bShutDown = false;
        this.nServerType = 0;
        this.nState = 0;
    }

    public static get instance(): ServerEngine {
        if (ServerEngine._instance == null) {
            ServerEngine._instance = new ServerEngine();
        }
        return ServerEngine._instance;
    }

    public initServer(setup:ServerEngineSetup):void{
        this.nServerType = setup.nServerType;
        this.nState = ServerEngineState.eOnInit;
        this.fServerDisconnected = setup.fServerDisconnected;
        this.fServerRegistered = setup.fServerRegistered;
        this.fServerConnected = setup.fServerConnected;
        this.fServerStarted = setup.fServerStarted;
        this.fServerListener = setup.fServerListener;

        RunLoop.instance.add(TimerManager.startGlobalTimer);

        ServerManager.instance.startWork(setup.nServerType);

        this.serverStart();
    }

    private serverStart():void{
        let tcpServer = net.createServer(function(sock){
            console.log("44444444444444")
        });
        tcpServer.on('error', function(err){
            console.log(err)
        });

        let serverName = ServerType[this.nServerType] + "Server";
        let config = ServerCfg.getServerConf(serverName);

        let nPort = config.ListenPort;
        tcpServer.listen(8000, config.ListenOn, function(){
            console.log("listen success");
        });

        while(true){
            
        }
    }


    public shutDownRequested():boolean{
        return this.bShutDown;
    }

    private shutDown():void{
        this.bShutDown = true;
    }

}