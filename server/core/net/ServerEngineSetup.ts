

export enum ServerType{
    Center = 1,
    Log,
    DB,
    Logic,
}

export enum ServerEngineState{
    eOnInit,
    eOnConnectToCenter,
    eOnGettingServerList,
    eOnRegisterToServers,
    eOnServices,
    eOnDestroy,
}

export interface ServerEngineSetup{
    nServerType:number;
    fServerDisconnected?:Function;
    fServerRegistered?:Function;
    fServerConnected?:Function;
    fServerStarted?:Function;
    fServerListener?:Function;
}

export interface StandaloneServerSetup{
    sServerIp:string;
    nServerPort:number;
    nPingTimeOut:number;
    fClientDisconnected?:Function;
    fClientConnected?:Function;
    fServerListener?:Function;
    fMessage?:Function;
}

export interface StandloneClientSetup{
    sServerIp:string;
    nServerPort:number;
    nConnectTimeOut:number;
    nPingTimeOut:number;
    fServerDisconnected?:Function;
    fServerConnected?:Function;
}