import {ServerType,ServerEngineSetup,ServerEngineState} from '../../core/net/ServerEngineSetup';
import {Server} from './Server';
import {RunLoop} from '../utils/RunLoop';

export class ServerManager{
    private static _instance:ServerManager;
    private nServerType:number;
    private vDepends:number[] = [];
    private nRunLoopId:number;
    private center:any = null;
    private db:any = null;
    private log:any = null;
    private logic:any = null;
    private allServers:{[id:number]:any} = {};
    private serverRemoved:Server[] = [];
    constructor(){
        this.nServerType = 0;
        this.nRunLoopId = 0;
    }

    public static get instance():ServerManager{
        if(!ServerManager._instance){
            ServerManager._instance = new ServerManager();
        }
        return ServerManager._instance;
    }

    public startWork(nType:number):void{
        this.nServerType = nType;
        switch(this.nServerType){
            case ServerType.Center:
                break;
            case ServerType.DB:
                this.vDepends.push(ServerType.Log);
                break;
            case ServerType.Log:
                break;
            case ServerType.Logic:
                this.vDepends.push(ServerType.Log);
                this.vDepends.push(ServerType.DB);
                break;
            default:
                break;
        }
        this.nRunLoopId = RunLoop.instance.add(this.doWork);
    }

    private doWork():void{
        // console.log("===========");
    }


}