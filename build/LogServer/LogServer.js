"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const App_1 = require("../core/App");
const ServerEngineSetup_1 = require("../core/net/ServerEngineSetup");
const ServerEngine_1 = require("../core/net/ServerEngine");
const serverCfg_1 = require("../core/cfg/serverCfg");
const TimerManager_1 = require("../core/utils/TimerManager");
class LogServer extends App_1.App {
    constructor() {
        super();
        this.quit = false;
    }
    onInit() {
        console.log("start init logserver");
        let serverAppSetup = {};
        serverAppSetup.nServerType = ServerEngineSetup_1.ServerType.Log;
        serverAppSetup.fServerRegistered = this.onServerRegistered;
        ServerEngine_1.ServerEngine.instance.initServer(serverAppSetup);
        let config = serverCfg_1.ServerCfg.getServerConf("LogServer");
        TimerManager_1.TimerManager.registerTimer(1000, this, this.test, null, false);
        return App_1.AppState.eRunning;
    }
    test() {
        console.log("2222222 %d", 1);
    }
    onRunning() {
        this.quit = this.quit || ServerEngine_1.ServerEngine.instance.shutDownRequested();
        return this.quit ? App_1.AppState.eCleanup : App_1.AppState.eRunning;
    }
    onCleanup() {
        console.log("logserver cleanup");
        return App_1.AppState.eDestroy;
    }
    onServerRegistered() {
    }
}
exports.LogServer = LogServer;
//# sourceMappingURL=LogServer.js.map