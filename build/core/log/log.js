"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const log4js = require("log4js");
let logCfg = require('../../../cfg/log4js.json');
class Log {
    constructor() {
        this.sLogName = "testServer";
    }
    static get instance() {
        if (Log._instance == null) {
            Log._instance = new Log();
        }
        return Log._instance;
    }
    initLog(sLogName) {
        this.sLogName = sLogName;
        log4js.configure(logCfg);
    }
    log(logType, message, ...args) {
        let logger = log4js.getLogger(this.sLogName);
        if (logger) {
            switch (logType) {
                case 'trace':
                    logger.trace(message, args);
                    break;
                case 'debug':
                    logger.debug(message, args);
                    break;
                case 'info':
                    logger.info(message, args);
                    break;
                case 'warn':
                    logger.warn(message, args);
                    break;
                case 'error':
                    logger.error(message, args);
                    break;
                case 'fatal':
                    logger.fatal(message, args);
                    break;
                default:
                    return;
            }
        }
    }
    trace(message, ...args) {
        this.log('trace', message, args);
    }
    debug(message, ...args) {
        this.log('debug', message, args);
    }
    info(message, ...args) {
        this.log('info', message, args);
    }
    warn(message, ...args) {
        this.log('warn', message, args);
    }
    error(message, ...args) {
        this.log('error', message, args);
    }
    fatal(message, ...args) {
        this.log('fatal', message, args);
    }
}
exports.Log = Log;
//# sourceMappingURL=log.js.map