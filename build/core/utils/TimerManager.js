"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TimerContext {
    constructor() {
        this.cb = function () { };
        this.args = undefined;
        this.expireTime = 0;
        this.interval = 0;
        this.loop = false;
        this.deleted = false;
        this.expiatory = false;
    }
}
class TimerManager {
    constructor() {
        this.timeList = [];
        this.lastTime = 0;
    }
    static get instance() {
        if (!TimerManager._instance) {
            TimerManager._instance = new TimerManager();
        }
        return TimerManager._instance;
    }
    static registerTimer(time, cbObject, cb, args, loop, expiatory) {
        return TimerManager.instance.registerTimer(time, cbObject, cb, args, loop, expiatory);
    }
    static unregisterTimer(handler) {
        return TimerManager.instance.unregisterTimer(handler);
    }
    static startGlobalTimer() {
        TimerManager.instance.timerCallBack();
    }
    static sleep(nSleepTime) {
        let nStartTime = Date.now();
        while (Date.now() - nStartTime <= nSleepTime) {
        }
    }
    registerTimer(time, cbObject, cb, args, loop, expiatory) {
        let context = new TimerContext();
        context.expireTime = Date.now() + time;
        context.interval = time;
        context.cbObject = cbObject;
        context.cb = cb;
        context.args = args;
        context.loop = loop;
        context.expiatory = expiatory;
        this.timeList.push(context);
        return context;
    }
    unregisterTimer(handler) {
        let context = (handler);
        if (!context.deleted) {
            let index = this.timeList.indexOf(context);
            if (index >= 0) {
                context.deleted = true;
                return true;
            }
        }
        return false;
    }
    timerCallBack() {
        let now = Date.now();
        let nDiffTime = now - this.lastTime;
        let nCount = this.timeList.length;
        if (nDiffTime < 1000 || nCount <= 0) {
            return;
        }
        this.lastTime = now;
        let context;
        for (let i = nCount - 1; i >= 0; --i) {
            context = this.timeList[i];
            if (context.deleted) {
                this.timeList.splice(i, 1);
            }
            else if (context.expireTime <= now) {
                if (!context.args || !context.args.unshift) {
                    context.cb.call(context.cbObject, context.args);
                }
                else {
                    context.cb.apply(context.cbObject, context.args);
                }
                if (!context.deleted && context.loop) {
                    if (context.expiatory) {
                        context.expireTime += context.interval;
                        let c = 500;
                        while (context.expireTime < now && c > 0) {
                            if (!context.args || !context.args.unshift) {
                                context.cb.call(context.cbObject, context.args);
                            }
                            else {
                                context.cb.apply(context.cbObject, context.args);
                            }
                            context.expireTime += context.interval;
                            --c;
                        }
                    }
                    else {
                        context.expireTime = now + context.interval;
                    }
                }
                else {
                    context.deleted = true;
                }
            }
        }
    }
}
exports.TimerManager = TimerManager;
//# sourceMappingURL=TimerManager.js.map