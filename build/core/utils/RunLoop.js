"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class RunLoop {
    constructor() {
        this.mCallBacks = {};
        this.mToAdd = {};
        this.vToRemove = {};
        this.nCurId = 0;
    }
    static get instance() {
        if (!RunLoop._instance) {
            RunLoop._instance = new RunLoop();
        }
        return RunLoop._instance;
    }
    run() {
        this.remCallBack();
        this.addCallBack();
        for (let id in this.mCallBacks) {
            if (this.mCallBacks[id]) {
                this.mCallBacks[id]();
            }
        }
        this.remCallBack();
        this.addCallBack();
    }
    add(callBack) {
        let newId = ++this.nCurId;
        this.mToAdd[newId] = callBack;
        return newId;
    }
    remove(id) {
        if (!this.vToRemove[id] && (this.mCallBacks[id] || this.mToAdd[id])) {
            this.vToRemove[id] = id;
        }
    }
    addCallBack() {
        for (let id in this.mToAdd) {
            let cb = this.mToAdd[id];
            this.mCallBacks[id] = cb;
        }
        this.mToAdd = {};
    }
    remCallBack() {
        for (let index in this.vToRemove) {
            let nLoopId = this.vToRemove[index];
            if (this.mCallBacks[nLoopId]) {
                delete this.mCallBacks[nLoopId];
            }
            else if (this.mToAdd[nLoopId]) {
                delete this.mToAdd[nLoopId];
            }
        }
        this.vToRemove = {};
    }
}
exports.RunLoop = RunLoop;
//# sourceMappingURL=RunLoop.js.map