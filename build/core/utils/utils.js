"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const serverCfg_1 = require("../cfg/serverCfg");
class Utils {
    constructor() {
    }
    static getCurTime() {
        return Math.floor(Date.now() / 1000);
    }
    static getCurMsTime() {
        return Date.now();
    }
    static genGuid(nSeriesId) {
        let nServerId = serverCfg_1.ServerCfg.getServerId();
        let nCurTime = this.getCurTime();
        return (nServerId << 52) | (nSeriesId << 32) | (nCurTime << 0);
    }
    static genServerId(nSeriesId, nType) {
        let nServerId = serverCfg_1.ServerCfg.getServerId();
        return (nServerId << 52) | (nSeriesId << 32) | (nType << 0);
    }
    static isTheSameDayByNow(nTimePoint, nSecond) {
        let date = new Date();
        let offset = date.getTimezoneOffset() * 60;
        let nCurTime = this.getCurTime();
        let day1 = (nTimePoint - offset - nSecond) / 86400;
        let day2 = (nCurTime - offset - nSecond) / 86400;
        if (Math.floor(day1) === Math.floor(day2)) {
            return true;
        }
        else {
            return false;
        }
    }
    static contains(array, n) {
        for (let i = 0; i < array.length; i++) {
            if (array[i] == n) {
                return true;
            }
        }
        return false;
    }
    static make_random(n, min, max) {
        let nums = [];
        if (n < 0 || min > max) {
            return nums;
        }
        if (n > (max - min + 1)) {
            for (let i = min; i <= max; i++) {
                nums.push(i);
            }
            return nums;
        }
        if (min <= max) {
            while (true) {
                if (nums.length >= n) {
                    break;
                }
                let num = Math.floor(min + Math.random() * (max - min + 1));
                if (!Utils.contains(nums, num)) {
                    nums.push(num);
                }
            }
        }
        return nums;
    }
    static make_one_random(min, max) {
        let num = -1;
        if (min <= max) {
            num = Math.floor(min + Math.random() * (max - min + 1));
        }
        return num;
    }
}
exports.Utils = Utils;
//# sourceMappingURL=utils.js.map