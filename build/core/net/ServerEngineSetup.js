"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ServerType;
(function (ServerType) {
    ServerType[ServerType["Center"] = 1] = "Center";
    ServerType[ServerType["Log"] = 2] = "Log";
    ServerType[ServerType["DB"] = 3] = "DB";
    ServerType[ServerType["Logic"] = 4] = "Logic";
})(ServerType = exports.ServerType || (exports.ServerType = {}));
var ServerEngineState;
(function (ServerEngineState) {
    ServerEngineState[ServerEngineState["eOnInit"] = 0] = "eOnInit";
    ServerEngineState[ServerEngineState["eOnConnectToCenter"] = 1] = "eOnConnectToCenter";
    ServerEngineState[ServerEngineState["eOnGettingServerList"] = 2] = "eOnGettingServerList";
    ServerEngineState[ServerEngineState["eOnRegisterToServers"] = 3] = "eOnRegisterToServers";
    ServerEngineState[ServerEngineState["eOnServices"] = 4] = "eOnServices";
    ServerEngineState[ServerEngineState["eOnDestroy"] = 5] = "eOnDestroy";
})(ServerEngineState = exports.ServerEngineState || (exports.ServerEngineState = {}));
//# sourceMappingURL=ServerEngineSetup.js.map