"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ServerEngineSetup_1 = require("../../core/net/ServerEngineSetup");
const TimerManager_1 = require("../../core/utils/TimerManager");
const RunLoop_1 = require("../utils/RunLoop");
const ServerManager_1 = require("./ServerManager");
const net = require("net");
const serverCfg_1 = require("../../core/cfg/serverCfg");
class ServerEngine {
    constructor() {
        this.bShutDown = false;
        this.nServerType = 0;
        this.nState = 0;
    }
    static get instance() {
        if (ServerEngine._instance == null) {
            ServerEngine._instance = new ServerEngine();
        }
        return ServerEngine._instance;
    }
    initServer(setup) {
        this.nServerType = setup.nServerType;
        this.nState = ServerEngineSetup_1.ServerEngineState.eOnInit;
        this.fServerDisconnected = setup.fServerDisconnected;
        this.fServerRegistered = setup.fServerRegistered;
        this.fServerConnected = setup.fServerConnected;
        this.fServerStarted = setup.fServerStarted;
        this.fServerListener = setup.fServerListener;
        RunLoop_1.RunLoop.instance.add(TimerManager_1.TimerManager.startGlobalTimer);
        ServerManager_1.ServerManager.instance.startWork(setup.nServerType);
        this.serverStart();
    }
    serverStart() {
        let tcpServer = net.createServer(function (sock) {
            console.log("44444444444444");
        });
        tcpServer.on('error', function (err) {
            console.log(err);
        });
        let serverName = ServerEngineSetup_1.ServerType[this.nServerType] + "Server";
        let config = serverCfg_1.ServerCfg.getServerConf(serverName);
        let nPort = config.ListenPort;
        tcpServer.listen(8000, config.ListenOn, function () {
            console.log("listen success");
        });
        while (true) {
        }
    }
    shutDownRequested() {
        return this.bShutDown;
    }
    shutDown() {
        this.bShutDown = true;
    }
}
exports.ServerEngine = ServerEngine;
//# sourceMappingURL=ServerEngine.js.map