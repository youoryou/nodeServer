"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ServerEngineSetup_1 = require("../../core/net/ServerEngineSetup");
const RunLoop_1 = require("../utils/RunLoop");
class ServerManager {
    constructor() {
        this.vDepends = [];
        this.center = null;
        this.db = null;
        this.log = null;
        this.logic = null;
        this.allServers = {};
        this.serverRemoved = [];
        this.nServerType = 0;
        this.nRunLoopId = 0;
    }
    static get instance() {
        if (!ServerManager._instance) {
            ServerManager._instance = new ServerManager();
        }
        return ServerManager._instance;
    }
    startWork(nType) {
        this.nServerType = nType;
        switch (this.nServerType) {
            case ServerEngineSetup_1.ServerType.Center:
                break;
            case ServerEngineSetup_1.ServerType.DB:
                this.vDepends.push(ServerEngineSetup_1.ServerType.Log);
                break;
            case ServerEngineSetup_1.ServerType.Log:
                break;
            case ServerEngineSetup_1.ServerType.Logic:
                this.vDepends.push(ServerEngineSetup_1.ServerType.Log);
                this.vDepends.push(ServerEngineSetup_1.ServerType.DB);
                break;
            default:
                break;
        }
        this.nRunLoopId = RunLoop_1.RunLoop.instance.add(this.doWork);
    }
    doWork() {
    }
}
exports.ServerManager = ServerManager;
//# sourceMappingURL=ServerManager.js.map