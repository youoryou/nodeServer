"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("./utils/utils");
const RunLoop_1 = require("./utils/RunLoop");
var AppState;
(function (AppState) {
    AppState[AppState["eConstruct"] = 0] = "eConstruct";
    AppState[AppState["eEnqueuePreload"] = 1] = "eEnqueuePreload";
    AppState[AppState["ePreloading"] = 2] = "ePreloading";
    AppState[AppState["eInit"] = 3] = "eInit";
    AppState[AppState["eRunning"] = 4] = "eRunning";
    AppState[AppState["eCleanup"] = 5] = "eCleanup";
    AppState[AppState["eDestroy"] = 6] = "eDestroy";
    AppState[AppState["eBlocked"] = 7] = "eBlocked";
    AppState[AppState["eNumAppStates"] = 8] = "eNumAppStates";
    AppState[AppState["eInvalidAppState"] = 9] = "eInvalidAppState";
})(AppState = exports.AppState || (exports.AppState = {}));
class App {
    constructor() {
        this.curState = AppState.eConstruct;
        this.nextState = AppState.eInvalidAppState;
        this.quitRequested = false;
        this.suspendRequest = false;
        this.blockers = [];
    }
    requestSuspend() {
        this.nextState = AppState.eCleanup;
        this.suspendRequest = true;
    }
    requestQuit() {
        if (this.curState == AppState.eRunning) {
            this.nextState = AppState.eCleanup;
        }
        else {
            this.nextState = AppState.eDestroy;
        }
        this.quitRequested = true;
    }
    onConstruct() {
        console.log("super construct");
        return AppState.eEnqueuePreload;
    }
    onEnqueuePreload() {
        console.log("super enqueuepreload");
        return AppState.ePreloading;
    }
    onPreloading() {
        console.log("super superpreloading");
        return AppState.eInit;
    }
    onInit() {
        console.log("super init");
        return AppState.eRunning;
    }
    onRunning() {
        console.log("super running");
        return AppState.eCleanup;
    }
    onCleanup() {
        console.log("super cleanup");
        if (this.quitRequested) {
            return AppState.eDestroy;
        }
        else if (this.suspendRequest) {
            this.addBlocker(AppState.eInit);
            return AppState.eInit;
        }
        else {
            return AppState.eDestroy;
        }
    }
    onDestroy() {
        console.log("super destroy");
        return AppState.eInvalidAppState;
    }
    addBlocker(nState) {
        if (!utils_1.Utils.contains(this.blockers, nState)) {
            this.blockers.push(nState);
        }
    }
    removeBlocker(nState) {
        let nIndex = this.blockers.indexOf(nState);
        if (nIndex > -1) {
            this.blockers.slice(nIndex, 1);
        }
    }
    startMainLoop() {
        console.log("start main loop");
        while (this.curState != AppState.eInvalidAppState) {
            this.onFrame();
        }
    }
    onFrame() {
        if ((this.nextState != AppState.eInvalidAppState) && (this.nextState != this.curState)) {
            if (utils_1.Utils.contains(this.blockers, this.nextState)) {
                if (this.curState != AppState.eBlocked) {
                    this.curState = AppState.eBlocked;
                }
            }
            else {
                this.curState = this.nextState;
                this.nextState = AppState.eInvalidAppState;
            }
        }
        if (AppState.eBlocked == this.curState) {
        }
        else {
            RunLoop_1.RunLoop.instance.run();
            switch (this.curState) {
                case AppState.eConstruct:
                    this.nextState = this.onConstruct();
                    break;
                case AppState.eEnqueuePreload:
                    this.nextState = this.onEnqueuePreload();
                    break;
                case AppState.ePreloading:
                    this.nextState = this.onPreloading();
                    break;
                case AppState.eInit:
                    this.nextState = this.onInit();
                    break;
                case AppState.eRunning:
                    this.nextState = this.onRunning();
                    break;
                case AppState.eCleanup:
                    this.nextState = this.onCleanup();
                    break;
                case AppState.eDestroy:
                    this.nextState = this.onDestroy();
                    this.curState = AppState.eInvalidAppState;
                    break;
                default:
                    break;
            }
        }
    }
}
exports.App = App;
//# sourceMappingURL=App.js.map