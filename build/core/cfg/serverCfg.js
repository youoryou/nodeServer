"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let cfg = require('../../../cfg/serverConfig.json');
class ServerCfg {
    constructor() {
    }
    static getServerConf(sServer) {
        return cfg[sServer];
    }
    static getServerId() {
        return cfg.ServerId;
    }
}
exports.ServerCfg = ServerCfg;
//# sourceMappingURL=serverCfg.js.map