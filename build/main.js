"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const App_1 = require("./core/App");
const utils_1 = require("./core/utils/utils");
const log_1 = require("./core/log/log");
const LogServer_1 = require("./LogServer/LogServer");
log_1.Log.instance.initLog("testServer");
log_1.Log.instance.trace("11111111111111");
log_1.Log.instance.debug("22222222222");
log_1.Log.instance.info("333333333333");
log_1.Log.instance.warn("4444444444444");
log_1.Log.instance.error('55555555555555');
log_1.Log.instance.fatal('66666666666666');
let a = utils_1.Utils.make_random(3, 1, 3);
a.forEach((i) => {
    log_1.Log.instance.info(i);
});
let b = utils_1.Utils.make_one_random(1, 2);
log_1.Log.instance.info(b);
console.log(App_1.AppState[App_1.AppState.eConstruct]);
let a1 = utils_1.Utils.genGuid(1);
if (utils_1.Utils.isTheSameDayByNow(utils_1.Utils.getCurTime(), 5 * 60 * 60)) {
    console.log("the same day");
}
process.on('uncaughtException', function (err) {
    log_1.Log.instance.error('Caught exception: %s' + err.stack);
});
let logServer = new LogServer_1.LogServer();
logServer.startMainLoop();
//# sourceMappingURL=main.js.map